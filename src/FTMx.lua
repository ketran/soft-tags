--[[
    A neural factor network
    author: Ke Tran <m.k.tran@uva.nl>
--]]

require 'cutorch'
require 'cunn'
require 'optim'


local FTM = torch.class('FTM')

function FTM:__init(config)
    local vocab_size = config.vocab_size
    local emb_size = config.emb_size or 192
    local ninputs = config.ninputs
    local nhids = config.nhids
    local noutputs = config.noutputs
    local alpha = config.alpha
    self.coefL2 = config.coefL2

    -- re-initialize following Delving Deep into Rectifiers paper
    -- turn off for now since the network is quite shallow
    function reset_std(llayer)
        local ninputs = llayer.weight:size(2)
        local stdv = math.sqrt(2.0/ninputs)
        llayer.weight:randn(llayer.weight:size()):mul(stdv)
        llayer.bias:zero()
    end
    -- creating network
    local lm = nn.Sequential()
    lm:add(nn.LookupTable(vocab_size, emb_size))
    lm:get(1):reset(1e-2)
    lm:add(nn.View(ninputs))
    lm:add(nn.Linear(ninputs, nhids))

    --lm:add(nn.ReLU(true)) -- in-place
    lm:add(nn.PReLU())
    lm:get(4).weight:fill(1e-2)
    lm:add(nn.Linear(nhids, noutputs))
    lm:cuda()
    self.lm = lm

    self.p = config.p or 0.1
    print('p: ' .. self.p)
    alpha = alpha/self.p
    local logz = nn.Sequential()
    logz:add(nn.Exp())
    logz:add(nn.Sum(2))
    logz:add(nn.Log())
    logz:add(nn.Square())
    logz:add(nn.MulConstant(alpha))
    logz:cuda()
    self.logz = logz

    -- for optimization
    self.parameters, self.gradParameters = self.lm:getParameters()
    -- for fast training
    self.b_size = config.b_size or 128
    self.grad_z = torch.ones(self.b_size):cuda()
    self.logSM = nn.LogSoftMax():cuda()
    -- objective function
    self.criterion = nn.ClassNLLCriterion():cuda()
    -- doing optimization with optim
    self.state = {}
    --self.config = {learningRate = 1e-3, beta1 = 0.9, beta2 = 0.999, epsilon = 1e-8, lambda = 1-1e-8}
    self.config = {learningRate = 3e-2}
end

function FTM:init_glove(glove_v, vocab, scale)
    for line in io.lines(glove_v) do
        local cols = stringx.split(line, ' ')
        local w = cols[1]
        local v = {}
        for i=2,#cols do v[#v + 1] = cols[i] end
        local v_cuda = torch.Tensor(v):mul(scale):cuda()
        self.lm:get(1).weight[vocab[w]]:copy(v_cuda)
    end
end


function FTM:load_net(model)
    self.lm = torch.load(model)
end

function FTM:save_net(model)
    torch.save(model, self.lm)
end

function FTM:learn(input, target)
    local loss_z = 0
    local feval = function(x)
        if x ~= self.parameters then
            self.parameters:copy(x)
        end
        self.gradParameters:zero()
        local output = self.lm:forward(input)
        output:clamp(-50,50) -- stabilizing

        local log_z = self.logz:forward(output)
        loss_z = log_z:sum()/self.b_size

        local log_p = self.logSM:forward(output)
        local f = self.criterion:forward(log_p, target)
        local gradCriterion = self.criterion:backward(log_p, target)
        local df_do = self.logSM:backward(output, gradCriterion)
        local gradz = torch.ones(self.b_size):bernoulli(0.1):cuda()
        local dz_do = self.logz:backward(output, gradz)
        df_do:add(dz_do)
        df_do:clamp(-0.1, 0.1)

        self.lm:backward(input, df_do)
        self.gradParameters:add(self.parameters:clone():mul(self.coefL2))

        return f, self.gradParameters
    end

    local _, fx = optim.adagrad(feval, self.parameters, self.config, self.state)
    collectgarbage()
    return fx[1], loss_z
end
