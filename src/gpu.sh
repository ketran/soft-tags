#!/bin/bash
#!/bin/sh
#SBATCH --time=99:00:00
#SBATCH -N 1
#SBATCH -C TitanX
#SBATCH --gres=gpu:1
#SBATCH -o expr.out
#SBATCH -e expr.err
#SBATCH -J debug
module load cuda70/toolkit/7.0.28
th trainx.lua expr.it.conf
