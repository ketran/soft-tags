function load_vocab (start_id, vocab_file)
    local vocab, idx = {}, start_id
        for w in io.lines(vocab_file) do
            idx = idx + 1
            vocab[w] = idx
        end
     return vocab, idx, idx-start_id
end
