require 'FTMx'
require 'Feeder'
require 'xlua'
require 'utils'


local logfile = require 'logging.file'
penlight_config = require 'pl.config'
local pretty = require 'pl.pretty'


local conf = penlight_config.read(arg[1])
os.execute('mkdir -p ' .. sys.dirname(conf.log))
local logger = logfile(conf.log)

-- log the configuration
logger:info(pretty.write(conf))

function init_net(conf)
    local start_id = 0
    local tag_vocab, start_id, n_tags  = load_vocab(start_id, conf.tag_vocab)
    if conf.nhist_l >= 0 then
        lemma_vocab, start_id, n_lemmas = load_vocab(start_id, conf.lemma_vocab)
    end

    if conf.window >= 0 then
        src_vocab, start_id, n_words = load_vocab(start_id, conf.src_vocab)
    end

    local feeder_conf = {   corpus = conf.corpus,
                            b_size = conf.b_size, c_size = conf.c_size,
                            nhist_t = conf.nhist_t, nhist_l = conf.nhist_l,
                            window = conf.window,
                            src_vocab = src_vocab, lemma_vocab = lemma_vocab, tag_vocab = tag_vocab,
                        }

    local src_window = 0
    if conf.window >= 0 then
        src_window = 2*conf.window + 1
    end

    local ninputs = (conf.nhist_t + src_window + conf.nhist_l + 1) * conf.emb_size
    local noutputs = n_tags


    local net_config = {vocab_size = start_id,
                        emb_size = conf.emb_size, ninputs = ninputs, nhids = conf.nhids,
                        noutputs = noutputs, alpha = conf.alpha,
                        coefL2 = conf.coefL2, b_size = conf.b_size, p = conf.p
                    }


    logger:info((pretty.write(net_config)))
    local feeder = Feeder(feeder_conf)
    local ftm = FTM(net_config)

    if conf.glove then
        logger:info(string.format('Initializing with pre-trained Glove vectors: %s', conf.glove))
        ftm:init_glove(conf.glove, src_vocab, 0.1)
        --ftm:init_glove(conf.glove_t, tag_vocab, 1)
    end

    return feeder, ftm
end

local n_sents = conf.n_sents
local c_size = conf.c_size

local feeder, ftm = init_net(conf)

function train()
    -- start reading data
    feeder:reset()

    local nll, loss_z = 0, 0
    local b_counter, s_counter = 0, 0
    while true do
        local inputs, targets = feeder:get()
        if s_counter + c_size > n_sents then break end
        s_counter = s_counter + c_size
        for i=1,#inputs do
            b_counter = b_counter + 1
            local f, log_z = ftm:learn(inputs[i], targets[i])
            nll = nll + f
            loss_z = loss_z + log_z
        end
        collectgarbage()

        xlua.progress(s_counter, n_sents)
        logger:info(string.format('NLL: %.5f\t z: %.5f', nll/b_counter, math.exp(loss_z/b_counter)))
    end
end


function main()
    for iter=1, conf.max_iter do
        local info = string.format("\n\n%s\nIter: %d", string.rep('-',42), iter)
        logger:info(info, iter)
        train()
        local model = string.format('%s.lm.t7', conf.model)
        os.execute('mkdir -p ' .. sys.dirname(model))
        ftm:save_net(model)
        local ftm_model = string.format('%s.ftm.t7', conf.model)
        os.execute('mkdir -p ' .. sys.dirname(ftm_model))
        torch.save(ftm_model, ftm)
    end
end

main()
