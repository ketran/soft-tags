--[[
    This class is used to read the training data
    and convert it to the format that will be fed to neural network
    author: Ke Tran <m.k.tran>
    date: 15/04/2015
--]]

local Feeder = torch.class('Feeder')

local stringx = require 'pl.stringx'

function Feeder:__init(config)
    self.corpus = config.corpus
    self.b_size = config.b_size
    self.c_size = config.c_size
    self.reader = nil

    self.nhist_t = config.nhist_t or 4
    self.nhist_l = config.nhist_l or 4
    self.window = config.window or 3
    self.src_vocab = config.src_vocab or {}
    self.lemma_vocab = config.lemma_vocab or {}
    self.tag_vocab = config.tag_vocab or {}
end

function Feeder:reset()
    self.reader = io.lines(self.corpus)
end

function Feeder:get()
    local x, y = {}, {}
    local nhist_t = self.nhist_t
    local window = self.window
    for i=1, self.c_size do
        local line = self.reader()
        if not line then break end
        local src_sent, trg_sent, walign = unpack(stringx.split(line, ' ||| '))
        local a = stringx.split(walign, ' ')
        local s_toks, t_toks = stringx.split(src_sent, ' '), stringx.split(trg_sent, ' ')
        local lseq, tseq = {}, {}
        for _, tok in pairs(t_toks) do
            local lemma, tag = unpack(stringx.split(tok, '|'))
            lseq[#lseq + 1] = lemma
            tseq[#tseq + 1] = tag
        end

        for j=1,#a do
            local sid, tid = unpack(stringx.split(a[j], '-'))
            sid, tid  = sid + 0, tid + 0
            -- in this implementation, the vocabulary is small, so we predict for all of the tags
            local c = {}
            -- tag history
            for t=nhist_t,1,-1 do -- read the tag sequence from left to right
                local tag = tseq[tid - t] or '<s>'
                c[#c + 1] = self.tag_vocab[tag] or  self.tag_vocab['unk']
            end
            -- lemma history
            if self.nhist_l >= 0 then
                -- quick hack here, start from 0
                for t=self.nhist_l,0,-1 do -- read the lemma sequence from left to right
                    local lemma = lseq[tid - t] or '<s>'
                    c[#c + 1] = self.lemma_vocab[lemma] or self.lemma_vocab['unk']
                end
            end
            -- source side context
            if window >= 0 then
                -- left words
                for t=window,1,-1 do
                    local w = s_toks[sid - t] or '<s>'
                    c[#c + 1] = self.src_vocab[w] or self.src_vocab['unk']
                end
                -- aligned source word
                c[#c + 1] = self.src_vocab[ s_toks[sid] ] or self.src_vocab['unk']
                -- right words
                for t=1, window do
                    local w = s_toks[sid + t] or '</s>'
                    c[#c + 1] = self.src_vocab[w] or self.src_vocab['unk']
                end
            end
            -- we do not predict for unknown tag
            if tseq[tid] ~= 'unk' and self.tag_vocab[ tseq[tid] ] then
                x[#x + 1] = c
                y[#y + 1] = self.tag_vocab[tseq[tid]]
            end
        end
    end


    -- convert to tensor
    local inputs, targets = {}, {}
    local x_tensor, y_tensor = torch.Tensor(x), torch.Tensor(y)
    local n_examples = #x
    if n_examples == 0 then
        return inputs, targets
    end

    local shuffle = torch.randperm(n_examples):long()
    for i=1,n_examples,self.b_size do
        local nidx = math.min(i + self.b_size-1, n_examples)
        if nidx == n_examples then break end
        local sample = shuffle[{{i, nidx}}]
        inputs[#inputs + 1] = x_tensor:index(1, sample):cuda()
        targets[#targets + 1] = y_tensor:index(1, sample):cuda()
    end

    return inputs, targets
end
