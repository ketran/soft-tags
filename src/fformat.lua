--[[
    A heuristic fixing alignment link
    Ensure that each target word have exactly one corresponding source word aligned to it
    Then convert training data into a new format
    author Ke Tran <m.k.tran@uva.nl>
--]]

local stringx = require 'pl.stringx'


function re_align(n, str_a)
    -- `n`: number of tokens in target sentence
    -- `str_a`: string of alignment
    local a = {}
    for i=1,n do a[i] = {} end
    for _,link in pairs(stringx.split(str_a, ' ')) do
        local sid, tid = unpack(stringx.split(link, '-'))
        table.insert(a[tid+1], sid+1)
    end

    local null_a = {} -- null alignment
    for i=1,#a do
        if #a[i] == 0 then
            a[i] = 0
            null_a[#null_a + 1] = i
        else
            local m = math.floor(#a[i]/2) + 1
            a[i] = a[i][m] -- in case of multiple source words, take the middle one
        end
    end

    for _,i in pairs(null_a) do
        local j = 1
        while a[i] == 0  and j < #a do
            a[i] = a[i+j] or a[i-j] or 0
            j = j + 1
        end
    end
    local a_new = {}
    for i=1,#a do
        a_new[#a_new + 1] = string.format('%s-%s', a[i], i)
    end
    return table.concat(a_new, ' ')
end


function re_format(srcfile, trgfile, alignfile)
    -- take source file, target file and alignment file as input
    -- re-format the data: source sentence ||| target sentence ||| word alignment
    local src, trg, align = io.lines(srcfile), io.lines(trgfile), io.lines(alignfile)
    local fw = io.open('xxx', 'w')
    while true do
        local s,t,a = src(), trg(), align()
        if not a then break end
        local n = #stringx.split(t, ' ')
        local a_new = re_align(n, a)
        local x = string.format("%s ||| %s ||| %s", s,t,a_new)
        fw:write(x .. '\n')
    end
    fw:close()
end

-- main
re_format(arg[1], arg[2], arg[3])
