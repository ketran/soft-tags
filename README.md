# Soft-tags

This is an implementation of neural soft-tags model described in the following paper:

- Ke M. Tran, Arianna Bisazza and Christof Monz. **A Distributed Inflection Model for Translating into Morphologically Rich Languages**. *In Proceedings of MT-Summit 2015*.

## Requirements
Torch 7

## Example
The code is written for GPUs, you need GPUs to run the code. Once you have Torch installed and GPUs, you can run the following command

```
$ sbatch gpu.sh
```
A sample of configuration file is in `src/expr.it.conf`
